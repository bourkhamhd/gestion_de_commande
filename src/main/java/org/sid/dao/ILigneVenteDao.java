package org.sid.dao;


import org.sid.entities.LigneVente;

public interface ILigneVenteDao extends IGenericDao<LigneVente> {

}
