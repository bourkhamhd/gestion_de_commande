package org.sid.dao;

import org.sid.entities.Article;

public interface IArticleDao extends IGenericDao<Article>{

}
