package org.sid.dao;

import org.sid.entities.Category;

public interface ICategoryDao extends IGenericDao<Category> {

}
