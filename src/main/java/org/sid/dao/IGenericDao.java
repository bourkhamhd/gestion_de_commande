package org.sid.dao;

import java.util.List;

public interface IGenericDao<E>  {

	public E save (E entity);
	
	public E update (E entity);
	
	public E getById (Long id);

	public List<E> SelectAll();
	
	public List<E> SelectAll(String sortField , String sort);
	
	public void remove (Long id);
	
	public E findOne(String paramName , Object paramValue);
	
	public E findOne (String[] paramNames , Object[] paramValues);
	
	public int findCountBy(String paramName , String paramValue);
	
	
	
	
	
}
