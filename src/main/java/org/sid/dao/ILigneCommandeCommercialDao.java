package org.sid.dao;

import org.sid.entities.LigneCommandeCommercial;

public interface ILigneCommandeCommercialDao extends IGenericDao<LigneCommandeCommercial> {

}
