package org.sid.dao;

import org.sid.entities.Vente;

public interface IVenteDao extends IGenericDao<Vente> {

}
