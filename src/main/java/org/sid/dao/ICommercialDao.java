package org.sid.dao;

import org.sid.entities.Commercial;

public interface ICommercialDao extends IGenericDao<Commercial> {

}
