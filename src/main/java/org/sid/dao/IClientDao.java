package org.sid.dao;

import org.sid.entities.Client;

public interface IClientDao extends IGenericDao<Client> {

}
