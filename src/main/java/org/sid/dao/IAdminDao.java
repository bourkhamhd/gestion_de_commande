package org.sid.dao;

import org.sid.entities.Admin;
import org.springframework.data.rest.webmvc.RepositoryRestController;
@RepositoryRestController
public interface IAdminDao extends IGenericDao<Admin>  {

}
