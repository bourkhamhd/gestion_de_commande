package org.sid.dao;

import org.sid.entities.MouvementStock;

public interface IMouvementStockDao extends IGenericDao<MouvementStock> {

}
