package org.sid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionDeCommandeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionDeCommandeApplication.class, args);
	}

}
